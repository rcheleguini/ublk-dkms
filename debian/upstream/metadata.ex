# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/ublk-dkms/issues
# Bug-Submit: https://github.com/<user>/ublk-dkms/issues/new
# Changelog: https://github.com/<user>/ublk-dkms/blob/master/CHANGES
# Documentation: https://github.com/<user>/ublk-dkms/wiki
# Repository-Browse: https://github.com/<user>/ublk-dkms
# Repository: https://github.com/<user>/ublk-dkms.git
